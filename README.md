# Setting up Google Camera
To build Google Camera you have to build the package in your device tree (Example, in device.mk).
```bash
# Inherit Google Camera
$(call inherit-product-if-exists, vendor/Gcam/config.mk) 
```

# Credits
* [**Google Camera LMC mods**](https://t.me/gcamlm)
